AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

util.AddNetworkString("imp_derma")
util.AddNetworkString("imp_collect")
util.AddNetworkString("imp_msg")
util.AddNetworkString("imp_unlock_car")
util.AddNetworkString("imp_unlock_car_pay")


impoundedCars = impoundedCars or {}
--impoundedCars["76561198058562944"] = {}

-- Sets the players model and basic physics ect..
function ENT:Initialize()
	self:SetModel("models/Police.mdl")
	self:SetHullType(HULL_HUMAN);
	self:SetHullSizeNormal();
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:SetSolid(SOLID_BBOX)
	self:SetUseType(SIMPLE_USE)
	self:DropToFloor()
end

function ENT:AcceptInput(name, ply, caller)
	--if !table.HasValue(XYZShit.Jobs.Criminals.All, ply:Team()) then return end

	-- Basic checks
	if !ply:IsPlayer() then return end
	if ply:GetPos():Distance( self:GetPos() ) > 100 then return end

	if !impoundedCars[ply:SteamID64()] then
		net.Start("imp_msg")
			net.WriteString("You have no cars in the impound")
		net.Send(ply)
		return
	end

	/*
	if next(impoundedCars[ply:SteamID64()]) == nil then
		net.Start("imp_msg")
			net.WriteString("You have no cars in the impound")
		net.Send(ply)
		return
	end */

	-- Opens derma
	net.Start("imp_derma")
		net.WriteEntity(self)
		net.WriteTable(impoundedCars[ply:SteamID64()])
	net.Send(ply)
end

net.Receive("imp_collect", function(_, ply)
	local npc = net.ReadEntity()

	if !npc then return end
	if npc:GetClass() != "impound_npc" then return end
	if ply:GetPos():Distance(npc:GetPos()) > 300 then return end

	local spawn = npc:GetPos() + (npc:GetAngles():Forward()* 140)

	local car = ents.Create("prop_vehicle_jeep")
	car:SetPos(spawn)
	car:SetAngles(npc:GetAngles())
	car:SetModel(impoundedCars[ply:SteamID64()].model)
	car:SetKeyValue("vehiclescript", impoundedCars[ply:SteamID64()].script)
	car:Spawn()
	car:Activate()
	car:SetVehicleClass(impoundedCars[ply:SteamID64()].ent)

	if not ply:canAfford(1000) then return end
	ply:addMoney(-1000)

	car:keysOwn( ply )
	car.owner = ply

	impoundedCars[ply:SteamID64()] = nil
	
	ply.curVehicle = car
end)