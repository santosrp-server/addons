include("shared.lua")

surface.CreateFont( "impoundfont120", {
	font = "Calibri",
	size = 120,
	weight = 100
})
surface.CreateFont( "impoundfont40", {
	font = "Calibri",
	size = ScreenScale(15),
	weight = 100
})
surface.CreateFont( "impoundfont30", {
	font = "Calibri",
	size = ScreenScale(10),
	weight = 100
})

-- The function for blur panels
local blur = Material("pp/blurscreen")
local function BlurPanel(panel, amount)
	local x, y = panel:LocalToScreen(0, 0)
	local scrW, scrH = ScrW(), ScrH()
	surface.SetDrawColor(255, 255, 255)
	surface.SetMaterial(blur)
	for i = 1, 6 do
		blur:SetFloat("$blur", (i / 3) * (amount or 6))
		blur:Recompute()
		render.UpdateScreenEffectTexture()
		surface.DrawTexturedRect(x * -1, y * -1, scrW, scrH)
	end
end

-- This just draws the texst above the NPCs head, it pulls from config
function ENT:Draw()
	self:DrawModel()
	if self:GetPos():Distance(LocalPlayer():GetPos()) > 500 then return end
	local ang = self:GetAngles();

	ang:RotateAroundAxis(ang:Forward(), 90);
	ang:RotateAroundAxis(ang:Right(), -90);

	cam.Start3D2D(self:GetPos()+self:GetUp()*80, Angle(0, self:GetAngles().y+90, 90), 0.07);
		draw.SimpleTextOutlined("Impound", "impoundfont120", 0, 0, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(0,0,0,255))
	cam.End3D2D()
	cam.Start3D2D(self:GetPos()+self:GetUp()*80, Angle(180, self:GetAngles().y+90, -90), 0.07);
		draw.SimpleTextOutlined("Impound", "impoundfont120", 0, 0, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(0,0,0,255))
	cam.End3D2D()
end

local function chatMessage(msg)
	chat.AddText( Color(200, 60, 120), "[Impound]: ", Color(255, 255, 255),  msg)
end

net.Receive("imp_msg", function()
	chatMessage(net.ReadString())
end)

net.Receive("imp_derma", function()
	local ent = net.ReadEntity()
	local carTbl = net.ReadTable()

	PrintTable(carTbl)

	local frame = vgui.Create("DFrame")
	frame:SetSize(ScrH()*0.75, ScrH()*0.75)
	frame:Center()
	frame:MakePopup()
	frame:SetTitle("")
	frame.Paint = function(self, w, h)
        BlurPanel(frame, 3)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
        draw.RoundedBox(0, 0, 0, w, 25, Color(40,40,40))
        draw.SimpleText("Seems that your car has been impounded...", "impoundfont40", w/2, 50, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
        draw.SimpleText("Well I'm here to help you get it back!", "impoundfont40", w/2, 90, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	local shell = vgui.Create("DPanel", frame)
	shell:SetPos(0, 120)
	shell:SetSize(frame:GetWide(), frame:GetTall()-120)
	shell.Paint = function() end

	local car = vgui.Create("DModelPanel", shell)
	car:SetPos(0, 0)
	car:SetSize(shell:GetWide(), shell:GetTall()*0.75)
	car:SetModel(carTbl.model)

	-- *|* Credit: https://wiki.garrysmod.com/page/DModelPanel/SetCamPos
	local mn, mx = car.Entity:GetRenderBounds()
	local size = 0
	size = math.max( size, math.abs( mn.x ) + math.abs( mx.x ) )
	size = math.max( size, math.abs( mn.y ) + math.abs( mx.y ) )
	size = math.max( size, math.abs( mn.z ) + math.abs( mx.z ) )
	car:SetFOV( 45 )
	car:SetCamPos( Vector( size+4, size+4, size+4 ) )
	car:SetLookAt( ( mn + mx ) * 0.5 )
	-- *|*

	local content = vgui.Create("DPanel", shell)
	content:SetSize(shell:GetWide()-10, (shell:GetTall()*0.25)-10)
	content:SetPos(5, (shell:GetTall()*0.75)+5)
	content.Paint = function(self, w, h)
		draw.SimpleText("Reclaim car for $1,000?", "impoundfont40", w/2, h/4, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
 	end

 	local sell = vgui.Create("DButton", content)
 	sell:SetPos(0, content:GetTall()/2)
 	sell:SetSize(content:GetWide(), content:GetTall()/2)
 	sell:SetText("")
 	sell.Paint = function(self, w, h)
 		if self:IsHovered() then
 			draw.RoundedBox(0, 0, 0, w, h, Color(75, 150, 75))
 		else
 			draw.RoundedBox(0, 0, 0, w, h, Color(50, 100, 50))
 		end
 		draw.SimpleText("Collect car", "impoundfont40", w/2, h/2, Color(255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
 	end

 	sell.DoClick = function()
 		frame:Remove()
 		net.Start("imp_collect")
 			net.WriteEntity(ent)
 		net.SendToServer()
 	end

end)