AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

-- Sets the players model and basic physics ect..
function ENT:Initialize()
	self:SetModel("models/freeman/wheel_clamp.mdl")

	-- Basic physics and functionality
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:Wake()
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
end

function ENT:AcceptInput(name, ply, caller)
end

function ENT:LockCar(car, ply)
	car.clampLocked = true
	car.clampEnt = self
end

hook.Add("PlayerEnteredVehicle", "imp_enter_car", function(ply, car)
	if car.clampLocked then
		ply:ExitVehicle()
		ply:AddNote("Your car has been clamped, contact a parking official")
	end
end)

hook.Add("PhysgunPickup","DisallowClamp",function(ply,ent)
	if ent:GetClass() == "xyz_clamp_ent" then
		return false
	end
end)