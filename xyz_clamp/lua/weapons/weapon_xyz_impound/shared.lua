SWEP.PrintName = "Impounder"
SWEP.Author = "Owain Owjo"

SWEP.Slot = 0
SWEP.SlotPos = 4

SWEP.Spawnable = true
SWEP.ViewModel = Model("models/freeman/c_wheelclamp.mdl")
SWEP.WorldModel = "models/freeman/wheel_clamp.mdl"
SWEP.ViewModelFOV = 85
SWEP.UseHands = true

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Ammo = "none"

SWEP.DrawAmmo = false
SWEP.Base = "weapon_base"

SWEP.Secondary.Ammo = "none"

SWEP.HoldType = ""

function SWEP:Initialize()
end

function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire(CurTime()+3)
	local ply = self.Owner

	local car = ply:GetEyeTrace().Entity
	if not car:IsVehicle() then return end
	local attackment = car:LookupAttachment("wheel_fr")
	local wheel = car:GetAttachment(attackment)

	if ply:GetPos():Distance(car:GetPos()) > 300 then return end

	if car.clampLocked then
		if SERVER then
			ply:AddNote("This car is already clamped")
		end
		return
	end

	ply:GetViewModel():SendViewModelMatchingSequence(ply:GetViewModel():LookupSequence("throw"))
	if CLIENT then return end
	timer.Simple(1.5, function()

		if not car then return end
		if ply:GetPos():Distance(car:GetPos()) > 300 then return end

		if car:GetSpeed() > 10 then
			ply:AddNote("The vehicle is moving")
			return
		end

		local ent = ents.Create("xyz_clamp_ent")
		ent:SetPos(wheel.Pos + (wheel.Ang:Forward()*3) + (wheel.Ang:Right()*4))
		ent:SetAngles(Angle(wheel.Ang.x, wheel.Ang.y, wheel.Ang.z))
		ent:SetParent(car)
		ent:LockCar(car, ply)
		ent:Spawn()

		if IsValid(car:GetDriver()) then
			car:GetDriver():ExitVehicle()
		end

	end)
end

function SWEP:SecondaryAttack()
	self:SetNextSecondaryFire(CurTime()+3)
	local ply = self.Owner

	local car = ply:GetEyeTrace().Entity
	if not car:IsVehicle() then return end

	if ply:GetPos():Distance(car:GetPos()) > 300 then return end

	if !car.clampLocked then
		return
	end

	if SERVER then
		ply:AddNote("You have unclamped this car")
	end

	car.clampLocked = false
	if IsValid(car.clampEnt) then
		car.clampEnt:Remove()
	end

end

function SWEP:Reload()
end
