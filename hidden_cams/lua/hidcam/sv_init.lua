resource.AddWorkshop("1345755676")

HIDCAMS = HIDCAMS or {}
HIDCAMS.HIDDEN_CAMS = HIDCAMS.HIDDEN_CAMS or {}

hook.Add("PlayerDisconnect", "HiddenCameras::DelteeOnDisconnect",function(ply)
	if HIDCAMS.HIDDEN_CAMS[ply:SteamID()] then
		for k,v in pairs(HIDCAMS.HIDDEN_CAMS[ply] ) do
			v:Remove()
		end
		HIDCAMS.HIDDEN_CAMS[ply:SteamID()] = nil
	end
end)

local lastCheck = SysTime()
hook.Add("Think", "HiddenCameras::DeleteUnusedCameras", function()
	local wait = HIDCAMS.CONFIG.WaitToRemove
	if SysTime() - lastCheck > 10 then
		lastCheck = SysTime()

		for k,v in pairs(HIDCAMS.HIDDEN_CAMS) do
			if IsValid(k) and k:HasWeapon("hidcam_placer") then
				k.lastHeld = SysTime()
			end

			if v and (not IsValid(k) or (k.lastHeld and wait >= 0 and (math.floor(SysTime() - k.lastHeld) > wait))) then
				for _, c in pairs(v) do
					if IsValid(c) then
						c:Remove()
					end
				end
				HIDCAMS.HIDDEN_CAMS[k] = {}
			end
		end
	end
end)