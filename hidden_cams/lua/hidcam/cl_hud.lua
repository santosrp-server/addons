local toggleDraw = false;
local cams = {};
local lastUpdated;
hook.Add("OnPlayerChat", "ToggleHiddenCam", function(ply, text)
	if ply == LocalPlayer() and text == HIDCAMS.CONFIG.WallHackCmd and HIDCAMS.CONFIG.WallHackCheck(ply) then
		toggleDraw = not toggleDraw
	end
end)

hook.Add("HUDPaint", "DrawHiddenCams", function()
	if toggleDraw then
		if not HIDCAMS.CONFIG.WallHackCheck(LocalPlayer()) then
			toggleDraw = false
		end
		if not lastUpdated or SysTime()-lastUpdated > 2 then
			lastUpdated = SysTime()
			cams = {}
			for k,v in pairs(ents.GetAll()) do
				if v:GetClass() == "hidcam" then
					cams[#cams+1] = v
				end
			end
		end

		local owner;
		for k,v in pairs(cams) do
			if IsValid(v) then
				local pos = v:GetPos():ToScreen()
				surface.SetDrawColor(255,255,255)
				surface.DrawRect(pos.x-3, pos.y-3, 6, 6)
				local owner = v:GetCameraOwner()
				if IsValid(owner) then
					draw.SimpleText(owner.Nick and owner:Nick() or owner:Name(),"Trebuchet24",pos.x,pos.y+10,Color(255,255,255),TEXT_ALIGN_CENTER)
				end
			end
		end
	end
end)