HIDCAMS.CONFIG = HIDCAMS.CONFIG or {}
local CONFIG = HIDCAMS.CONFIG

CONFIG.FrameLimit = 20 -- How many frames can be drawn per second (smaller tablet FPS = higher FPS for the game)
CONFIG.MaxCameraPerPlayer = 5 -- How many cameras can a player can own
CONFIG.WaitToRemove = 0 -- How long (in seconds) to wait after the player does not have the tablet to delete the camera (-1 to disable)
CONFIG.StartingHP = 100 -- How much HP the camera starts out with on spawn
CONFIG.WallHackCmd = "/drawhiddencams"
CONFIG.WallHackCheck = function(ply)
	return ply:IsUserGroup("superadmin")
end

CONFIG.DeleteKey = KEY_X -- THe button to delete camera. Find the key here: https://wiki.garrysmod.com/page/Enums/KEY

CONFIG.Language = {
	["rotationmode"] = "Rotation Mode On",
	["zoominstruct"] = "W/S - Zoom In/Out",
	["cameranotfound"] = "No Camera Found",
	["instruct"] = "Instructions:",
	["placecamera"] = "Primary Click - Place Camera",
	["switchcamera"] = "Secondary Click - Switch Camera",
	["deletecamera"] = "X - Delete Camera",
	["rotatecamera"] = "R - Rotate Camera"
}