AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Hidden camera"
ENT.Author = "George Gruber"

function ENT:SetupDataTables()
	self:NetworkVar("Float", 1, "CameraHealth")
	self:NetworkVar("Entity", 1, "CameraOwner")
end

function ENT:Initialize()
	self:SetModel("models/customhq/hidcams/minicam.mdl")
	if SERVER then self:PhysicsInit(SOLID_VPHYSICS) end
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()
	if phys and phys:IsValid() then phys:Wake() end

	if SERVER then
		self:SetCameraHealth(HIDCAMS.CONFIG.StartingHP)
	end
end

function ENT:Draw()
	self:DrawModel()
	-- local ang = self:GetAngles()
	-- ang:RotateAroundAxis(ang:Forward(),90)
	-- ang:RotateAroundAxis(-ang:Right(), 90)
	-- local hp = (self:GetCameraHealth() or 100)
	-- local hpFact = hp/100
	-- cam.Start3D2D(self:GetPos() + self:GetAngles():Up()*2.5,ang, 0.1)
	-- 	surface.SetDrawColor(50,50,50)
	-- 	surface.DrawRect(-30,0,60,10)

	-- 	surface.SetDrawColor(Lerp(hpFact, 255, 0),Lerp(hpFact, 0, 255),0)
	-- 	surface.DrawRect(-30,0, Lerp(hpFact, 0, 60), 10)

	-- cam.End3D2D()
end

if SERVER then
	function ENT:OnTakeDamage(dmgInfo)
		local hp = self:GetCameraHealth()
		hp = hp - dmgInfo:GetDamage()

		if hp < 1 then
			self:Remove()
		else
			self:SetCameraHealth(hp)
		end
	end
end