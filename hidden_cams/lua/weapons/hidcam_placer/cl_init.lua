include("sh_ghost.lua")
include("shared.lua")


local drawer;
hook.Add("ShouldDrawLocalPlayer", "DrawPlayerOnHidCam", function(ply)
	if drawer then
		return drawer
	end
end)

local startx = -405
local starty = -252
local scrW = 810
local scrH = 510
local inputReceive = false
local mat = CreateMaterial("LocalTabletScreen", "UnLitGeneric", {})
local noise = surface.GetTextureID("effects/tvscreen_noise003a")
local overlay = Material("models/customhq/hidcams/tabletglass")
local rt = GetRenderTarget("TabletRT", scrW, scrH)
local camera
local lastDrawn = SysTime()

net.Receive("HiddenCamSwitchView", function(len)
	camera = net.ReadEntity()
end)

local function renderScreen()
	mat:SetTexture("$basetexture", rt)
	local pos, ang

	if IsValid(camera) then
		pos, ang = camera:GetPos(), camera:GetAngles()
		ang:RotateAroundAxis(ang:Up(), camera.angOffsetUp or 0)
		ang:RotateAroundAxis(ang:Right(), camera.angOffsetRight or 0)
		render.PushRenderTarget(rt)
		render.Clear(255, 255, 255, 255)
		cam.Start2D()
		drawer = true

		render.RenderView({
			origin = pos or Vector(773.512329, 1462.273438, 3.857243),
			angles = ang or Angle(0.151797, 142.777420, 0.000000),
			x = 0,
			y = 0,
			w = scrW,
			h = scrH,
			drawviewmodel = false,
			fov = camera.fov or 75
		})

		drawer = false
		cam.End2D()
		render.PopRenderTarget()
	end
end

function SWEP:PostDrawViewModel()
	local lang = HIDCAMS.CONFIG.Language
	local vm = LocalPlayer():GetViewModel()

	if not IsValid(vm) then
		return
	end

	local bone = vm:LookupBone("tablet")

	if (not bone) then
		return
	end

	local pos, ang = Vector(0, 0, 0), Angle(0, 0, 0)
	local m = vm:GetBoneMatrix(bone)

	if (m) then
		pos, ang = m:GetTranslation(), m:GetAngles()
		ang:RotateAroundAxis(ang:Right(), 180)
	else
		return
	end

	cam.Start3D2D(pos + (EyePos() - pos):GetNormalized() * 0.1, ang, 0.01)

	if IsValid(camera) then
		surface.SetDrawColor(255, 255, 255, 255)
		surface.SetMaterial(mat)
		surface.DrawTexturedRect(startx, starty, scrW, scrH)

		local maxHP = HIDCAMS.CONFIG.StartingHP
		local hp = (camera:GetCameraHealth() or maxHP)
		local hpFact = hp/maxHP
		surface.SetDrawColor(50,50,50)
		surface.DrawRect(startx+10,starty+scrH-10-20,100,20)

		surface.SetDrawColor(Lerp(hpFact, 255, 0),Lerp(hpFact, 0, 255),0)
		surface.DrawRect(startx+10,starty+scrH-10-20, Lerp(hpFact, 0, 100), 20)

		if inputReceive then
			surface.SetDrawColor(0, 0, 0, 150)
			surface.DrawRect(startx, starty, scrW, 32 + 5)
			surface.SetFont("DermaLarge")
			surface.SetTextColor(255, 255, 255, 255)
			surface.SetTextPos(startx + 20, starty + 5)
			surface.DrawText(lang["rotationmode"] or "Rotation Mode On")
			local w, h = surface.GetTextSize(lang["zoominstruct"] or "W/S - Zoom In/Out")
			surface.SetTextPos(startx + scrW - w - 20, starty + 5)
			surface.DrawText(lang["zoominstruct"] or "W/S - Zoom In/Out")
		end
	else
		surface.SetFont("DermaLarge")

		if math.floor(SysTime() % 2) == 0 then
			surface.SetTextColor(255, 0, 0, 255)
			surface.SetTextPos(startx + 50, starty + scrH / 2 - 32 / 2)
			surface.DrawText(lang["cameranotfound"] or "No Camera Found")
		end

		surface.SetTextColor(255, 255, 255)
		surface.SetTextPos(startx + scrW / 2, starty + scrH / 4)
		surface.DrawText(lang["instruct"] or "Instructions:")
		surface.SetTextPos(startx + scrW / 2, starty + scrH / 4 + 35 * 1)
		surface.DrawText(lang["placecamera"] or "Primary Click - Place Camera")
		surface.SetTextPos(startx + scrW / 2, starty + scrH / 4 + 35 * 2)
		surface.DrawText(lang["switchcamera"] or "Secondary Click - Switch Camera")
		surface.SetTextPos(startx + scrW / 2, starty + scrH / 4 + 35 * 3)
		surface.DrawText(lang["deletecamera"] or "X - Delete Camera")
		surface.SetTextPos(startx + scrW / 2, starty + scrH / 4 + 35 * 4)
		surface.DrawText(lang["rotatecamera"] or "R - Rotate Camera")
	end

	cam.End3D2D()
end

local reloadActive = false
local lastDeleted

function SWEP:Think()

	if not HIDCAMS.CONFIG or HIDCAMS.CONFIG.FrameLimit <= 0 or (not (lastDrawn and isnumber(lastDrawn))) or SysTime() - lastDrawn >= 1/HIDCAMS.CONFIG.FrameLimit then
		renderScreen()
		lastDrawn = SysTime()
	end
	if (not IsValid(self.GhostEntity)) then
		self:MakeGhostEntity("models/customhq/hidcams/minicam.mdl", Vector(0, 0, 0), Angle(0, 0, 0))
	end

	self:UpdateGhostEntity()

	if input.IsKeyDown(HIDCAMS.CONFIG.DeleteKey) and not IsValid(vgui.GetKeyboardFocus()) and (not (lastDeleted and isnumber(lastDeleted)) or SysTime() - lastDeleted > 0.5) then
		lastDeleted = SysTime()
		net.Start("DeleteCamera")
		net.SendToServer()
	end
end

function SWEP:OnRemove()
	self:ReleaseGhostEntity()
end

function SWEP:Holster()
	self:ReleaseGhostEntity()
end

function SWEP:Reload()
	if not input.IsKeyDown(KEY_R) then
		return
	end

	if self.ReloadingTime and SysTime() - self.ReloadingTime < 0.5 then
		return
	end

	self.ReloadingTime = SysTime()
	inputReceive = not inputReceive
	inputReceiveAngle = LocalPlayer():EyeAngles()
end

hook.Add("CreateMove", "HIDDEN_CAM_ZOOM_SCROLL", function(cmd)
	local wep = LocalPlayer():GetActiveWeapon()
	if inputReceive then
		if IsValid(camera) and wep and wep:GetClass() == "hidcam_placer" then
			local diff = inputReceiveAngle - cmd:GetViewAngles()
			camera.angOffsetRight = math.Clamp((camera.angOffsetRight or 0) + diff.p, -50, 50)
			camera.angOffsetUp = math.Clamp((camera.angOffsetUp or 0) - diff.y, -50, 50)

			if cmd:GetForwardMove() then
				camera.fov = math.Clamp((camera.fov or 75) - cmd:GetForwardMove() / 10000 / 10, 1, 75)
				cmd:ClearMovement()
			end

			cmd:SetViewAngles(inputReceiveAngle)
			cmd:SetMouseWheel(0)
		else
			inputReceive = false
		end
	end

	if wep.GhostEntity and wep:GetClass() != "hidcam_placer" then
		ReleaseGhostEntity()
	end
end)