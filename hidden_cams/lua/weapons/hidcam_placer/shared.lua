SWEP.PrintName			= "Hidden Camera Placer" -- This will be shown in the spawn menu, and in the weapon selection menu
SWEP.Author			= "George Gruber & CustomHQ" -- These two options will be shown when you have the weapon highlighted in the weapon selection menu
SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo		= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

SWEP.Weight			= 1
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Slot			= 1
SWEP.SlotPos			= 2
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= false

SWEP.ViewModel			= "models/customhq/hidcams/tablet_v.mdl"
SWEP.WorldModel			= "models/customhq/hidcams/tablet_w.mdl"
SWEP.UseHands = true

SWEP.ViewModelFOV = 54
