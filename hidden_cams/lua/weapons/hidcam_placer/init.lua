AddCSLuaFile("cl_init.lua")
AddCSLuaFile("sh_ghost.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString("HiddenCamSwitchView")
util.AddNetworkString("DeleteCamera")
function SWEP:Deploy()
	HIDCAMS.HIDDEN_CAMS[self:GetOwner()] = HIDCAMS.HIDDEN_CAMS[self:GetOwner()] or {}
end

function SWEP:PrimaryAttack()
	if HIDCAMS.HIDDEN_CAMS[self:GetOwner()] then
		local count = 0
		for k,v in pairs(HIDCAMS.HIDDEN_CAMS[self:GetOwner()]) do
			if IsValid(v) then
				count = count + 1
			end
		end
		if count >= HIDCAMS.CONFIG.MaxCameraPerPlayer then
			self:GetOwner():ChatPrint("You can only have "..HIDCAMS.CONFIG.MaxCameraPerPlayer.." cameras")
			return
		end
	end

	if not IsValid(self:GetOwner()) or not self:GetOwner().GetEyeTrace then return end
	
	local trace = self:GetOwner():GetEyeTrace()
	if ( !trace.Hit ) then return end

	if FPP and trace.Entity:EntIndex() != 0 then
		if not FPP.plyCanTouchEnt(self:GetOwner(), trace.Entity, "Toolgun") then return end
	end
	local ang = trace.HitNormal:Angle()


	if ang.y == 0 then
		ang.y = self:GetOwner():EyeAngles().y+180
	end

	if self:GetOwner():EyePos():DistToSqr(trace.HitPos) < 35000 then
		local camera = ents.Create("hidcam")
		camera:SetPos( trace.HitPos )
		camera:SetAngles( ang )
		camera:Spawn()
		camera:SetCameraOwner(self:GetOwner())

		if trace.Entity then
			constraint.Weld(camera,trace.Entity,camera:LookupBone("root"),trace.PhysicsBone	,0,true,true)
		end

		table.insert(HIDCAMS.HIDDEN_CAMS[self:GetOwner()],camera)
		self.curCam = camera
		timer.Simple(0.2, function()
			if IsValid(camera) then
				local phys = camera:GetPhysicsObject()
				if IsValid(phys) then
					if phys:IsPenetrating() then
						camera:Remove()
					else
						net.Start("HiddenCamSwitchView")
							net.WriteEntity(camera)
						net.Send(self:GetOwner())
					end
				end
			end
		end)
	end
end

function SWEP:SecondaryAttack()
	self:SelectNewCamera()
end

function SWEP:SelectNewCamera()
	// Make sure all of our cameras are valid
	local pure = {}
	local current = 1;
	for k,v in pairs(HIDCAMS.HIDDEN_CAMS[self:GetOwner()]) do
		if IsValid(v) then
			local t = table.insert(pure, v)
			if self.curCam == v then
				current = t
			end
		end
	end

	HIDCAMS.HIDDEN_CAMS[self:GetOwner()] = pure

	current = current + 1
	if current > #pure then
		current = 1
	end

	if pure[current] and IsValid(pure[current]) then
		self.curCam = pure[current]
		net.Start("HiddenCamSwitchView")
			net.WriteEntity(pure[current])
		net.Send(self:GetOwner())
	end

end

function SWEP:DeleteCurCam()
	if self.curCam and IsValid(self.curCam) then
		local curcam = self.curCam
		self.curCam:Remove()
		self:SelectNewCamera()
	end
end

net.Receive("DeleteCamera", function(len, ply)
	local wep = ply:GetWeapon("hidcam_placer")
	if wep then
		wep:DeleteCurCam()
	end
end)