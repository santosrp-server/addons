Thank you for using VCMod.

Feel free to contact me at: https://www.gmodstore.com/users/view/76561197989323181 or by email: freemmaann@gmail.com about any issues or even feature requests.

This comment is identical to the included "Readme.txt" file.


*****************************************************************
Installation:
*****************************************************************

Steps for installing VCMod ELS:

1) Once downloaded, right click on the downloaded .zip type file and select "Unarchive" or "Unrar" or "Extract" or "Unzip", depending on your software.

	Once done, you will receive a folder called "vcmod_els_autoupdate".

2) Place this folder into your "garrysmod/addons/" folder of your server or single-player game.

	The complete file path should look similar to this: "C:\Program Files (x86)\Steam\steamapps\common\GarrysMod\garrysmod\addons\vcmod_els_autoupdate\".

	If you have issues finding this, go to your Steam Library, right click on Garrysmod, Properties, Local Files and click the "Browse local files..." button. This will take you straight into the installation directory of the game.

3) If Garrysmod is running, restart the game itself. If this is done for a server, restart the server.


If you have followed these steps correctly VCMod will be fully functional and working for you.


*****************************************************************
Addon updates:
*****************************************************************

VCMod has its own automated updater. Which fetches the newest patches, vehicle data edits, language changes, etc. automatically. VCMod will not have to be updated manually.

At certain points there might be "massive" VCMod updates or the core code of the auto updater will be edited, which might include files that VCMod can not automatically update by itself. If this happens, administrators will be notified ingame about the pending update.

Full changelogs are available at: http://vcmod.org/changelog/?sel=els and http://vcmod.org/changelog/?sel=data


*****************************************************************
Beta branch:
*****************************************************************

VCMod has two branches: Public and Beta.

Public branch is used by every single VCMod buyer by default and is the most stable version available.
Beta branch is used to test out new features and get feedback. You will get "hands on" of what's being developed and what will soon be released. However, this branch might not be stable enough for general server use.

Steps to switch to the Beta branch:

1) Start the Server/Single player game. It must be active and VCMod fully functional on it.
2) Open the ingame console. You need to have access to the servers (your personal one, if in single player) console.
3) Enter "vc_usebeta 1" into the console ("vc_usebeta 0" of you want to switch to public branch, all without the quotes).
4) Restart the map. VCMod needs to fully reload for changes to take effect. Either restart the server/game. You can also force VCMod to change by switching to a new or reloading the map.

Switching to Beta branch is not permanent and you can switch back without any damages to the Public branch at any time.


*****************************************************************
Menu:
*****************************************************************

You can open the control menu by holding <C> and selecting "VCMod" or by entering this "vc_open_menu" in console or "!vcmod" in chat

All further options and controls are mentioned within the game.