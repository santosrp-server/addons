local RegisterVehiclePlates = LPlates.RegisterVehiclePlates

RegisterVehiclePlates( "Jeep",
	{
		pos = Vector( 0, 57, 30 ), --position of the plate (center)
		ang = Angle( 0, 180, 90 ), --angle of the plate
		scale = 0.035			   --scale (size) of the plate
	},
	{
		pos = Vector( 0, -100, 30 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

---- TDM VEHICLES ----

RegisterVehiclePlates( "audir8tdm",
	{
		pos = Vector( 0, 108.2, 20 ),
		ang = Angle( 0, 180, 80 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -104, 29.5 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "dbstdm",
	{
		pos = Vector( 0, 116.5, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -111, 37.3 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "m3e92tdm",
	{
		pos = Vector( 0, 112, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -110, 37.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)


RegisterVehiclePlates( "sparktdm",
	{
		pos = Vector( 0, 88.25, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -85.5, 39 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "hsvw247tdm",
	{
		pos = Vector( 0, 108, 19 ),
		ang = Angle( 0, 180, 84 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -108, 36.7 ),
		ang = Angle( 0, 0, 77 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "auditttdm",
	{
		pos = Vector( 3, 101.5, 20 ),
		ang = Angle( 0, 180, 85 ),
		scale = 0.035
	},
	{
		pos = Vector( 3, -94.7, 29.9 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "landrovertdm",
	{
		pos = Vector( 0, 118.5, 28 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -117, 48 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "crownvic_taxitdm",
	{
		pos = Vector( 3, 103, 20 ),
		ang = Angle( 0, 180, 85 ),
		scale = 0.035
	},
	{
		pos = Vector( 3, -94.7, 29.9 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "ceedtdm",
	{
		pos = Vector( 0, 107, 20 ),
		ang = Angle( 0, 180, 85 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -97.7, 34.5 ),
		ang = Angle( 0, 0, 75 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "hsvw247poltdm",
	{
		pos = Vector( 0, 108, 15.5 ),
		ang = Angle( 0, 180, 84 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -108, 36.7 ),
		ang = Angle( 0, 0, 77 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "cayennetdm",
	{
		pos = Vector( 0, 116.8, 32 ),
		ang = Angle( 0, 180, 84 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -115.3, 42 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "242turbotdm",
	{
		pos = Vector( 0, 121.2, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -111, 31 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "bustdm",
	{
		pos = Vector( -37, 262.5, 63 ),
		ang = Angle( 0, 183, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 26, -268, 44 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "scaniahtdm",
	{
		pos = Vector( 1.5, 120.36, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( -36, -121, 24 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "scanialtdm",
	{
		pos = Vector( 1.5, 122.4, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( -36, -119, 24 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "scaniamtdm",
	{
		pos = Vector( 1.5, 122.4, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( -36, -119.7, 24 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "supratdm",
	{
		pos = Vector( 0, 113.2, 17.8 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -111, 31 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "s5tdm",
	{
		pos = Vector( 0, 108, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -117, 38 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.035
	}
)

RegisterVehiclePlates( "bmwm5e60tdm",
	{
		pos = Vector( 0, 118.5, 24 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -113, 40 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "chargersrt8tdm",
	{
		pos = Vector( 0, 114, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -117, 27 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.025
	}
)

RegisterVehiclePlates( "300ctdm",
	{
		pos = Vector( 0, 124, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -124, 29.5 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "gt500tdm",
	{
		pos = Vector( 0, 116.2, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -115, 26.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "c4tdm",
	{
		pos = Vector( 0, 96, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -100, 25.7 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.025
	}
)

RegisterVehiclePlates( "civictypertdm",
	{
		pos = Vector( 0, 95, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -83, 33 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.025
	}
)

RegisterVehiclePlates( "sl65amgtdm",
	{
		pos = Vector( 0, 104.5, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -100, 32.2 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "impala96tdm",
	{
		pos = Vector( 0, 121, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -140, 35 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "camarozl1tdm",
	{
		pos = Vector( 0, 119, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -116, 34 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "chevellesstdm",
	{
		pos = Vector( 0, 126, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -122, 27 ),
		ang = Angle( 0, 0, 105 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "golf3tdm",
	{
		pos = Vector( 0, 101, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -94.5, 37 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "focusrstdm",
	{
		pos = Vector( 0, 101, 21.5 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -103, 38 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "hudhornettdm",
	{
		pos = Vector( 0, 130, 23 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -127, 24.5 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "crownvic_taxitdm",
	{
		pos = Vector( 0, 137.5, 20.5 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -113, 37.5 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "69camarotdm",
	{
		pos = Vector( 0, 118, 20 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -103.5, 26 ),
		ang = Angle( 0, 0, 115 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "ferrari250gttdm",
	{
		pos = Vector( 0, 103, 14 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -99, 26.5 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "ferrari512trtdm",
	{
		pos = Vector( 0, 101, 12 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -102, 19.5 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "458spidtdm",
	{
		pos = Vector( 0, 110, 14 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -110, 29 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "gt05tdm",
	{
		pos = Vector( 0, 114, 15 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -102.5, 30.5 ),
		ang = Angle( 0, 0, 110 ),
		scale = 0.037
	}
)

RegisterVehiclePlates( "rx8tdm",
	{
		pos = Vector( 0, 106, 25 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -104, 35.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.034
	}
)

RegisterVehiclePlates( "mx5tdm",
	{
		pos = Vector( 0, 99, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.031
	},
	{
		pos = Vector( 0, -91, 33.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.034
	}
)

RegisterVehiclePlates( "murcielagotdm",
	{
		pos = Vector( 0, 109, 14 ),
		ang = Angle( 0, 180, 105 ),
		scale = 0.029
	},
	{
		pos = Vector( 0, -107, 31.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.026
	}
)

RegisterVehiclePlates( "gallardotdm",
	{
		pos = Vector( 0, 109, 13 ),
		ang = Angle( 0, 180, 105 ),
		scale = 0.027
	},
	{
		pos = Vector( 0, -100, 17 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.026
	}
)

RegisterVehiclePlates( "350ztdm",
	{
		pos = Vector( 0, 102, 16.5 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -105, 27.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "c32amgtdm",
	{
		pos = Vector( 0, 112, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -108, 39 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "reventonrtdm",
	{
		pos = Vector( 0, 119, 14 ),
		ang = Angle( 0, 180, 120 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -113, 32 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "rs4avanttdm",
	{
		pos = Vector( 0, 121, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -105, 36 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "gtrtdm",
	{
		pos = Vector( 0, 124, 22 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -102, 30 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "507tdm",
	{
		pos = Vector( 0, -105, 29 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "veyronsstdm",
	{
		pos = Vector( 0, 108, 17 ),
		ang = Angle( 0, 180, 75 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -107, 28.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "veyrontdm",
	{
		pos = Vector( 0, 108, 17 ),
		ang = Angle( 0, 180, 75 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -107, 28.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "mitsu_evoxtdm",
	{
		pos = Vector( 0, 98, 24 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -118, 23 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "eb110tdm",
	{
		pos = Vector( 0, 108, 12 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -105, 33 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "colttdm",
	{
		pos = Vector( 0, 102.5, 25 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -90, 22 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "cooper65tdm",
	{
		pos = Vector( 0, 76, 25 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -76, 29.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.026
	}
)

RegisterVehiclePlates( "slsamgtdm",
	{
		pos = Vector( 0, 115, 16 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -113, 24.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "r34tdm",
	{
		pos = Vector( 0, 110.5, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -111, 23 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "c12tdm",
	{
		pos = Vector( 0, 109, 10 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -103, 18 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.030
	}
)

RegisterVehiclePlates( "m1tdm",
	{
		pos = Vector( 0, 104, 12 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -103, 34.5 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.032
	}
)

RegisterVehiclePlates( "997gt3tdm",
	{
		pos = Vector( 0, 109, 15 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -107, 21.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.03
	}
)

RegisterVehiclePlates( "TDM Subaru Impreza",
	{
		pos = Vector( 25, 112, 15 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 25, -99, 39 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.034
	}
)

RegisterVehiclePlates( "priustdm",
	{
		pos = Vector( 0, 116, 24 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -99, 39 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.034
	}
)

RegisterVehiclePlates( "sciroccotdm",
	{
		pos = Vector( 0, 99, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -95, 25.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "golfmk2tdm",
	{
		pos = Vector( 0, 113.5, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -106, 45 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "beetle67tdm",
	{
		pos = Vector( 0, -99, 21.5 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.028
	}
)

RegisterVehiclePlates( "coupe40tdm",
	{
		pos = Vector( 0, 113.5, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	}
)

RegisterVehiclePlates( "dodgeramtdm",
	{
		pos = Vector( 0, 114, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -114, 27 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "syclonetdm",
	{
		pos = Vector( 0, 114, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -114, 27 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "blazertdm",
	{
		pos = Vector( 0, 108, 20.5 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -106, 28 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "escaladetdm",
	{
		pos = Vector( 0, 125, 26 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -125, 44.25 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.033
	}
)

RegisterVehiclePlates( "gmcvantdm",
	{
		pos = Vector( 0, 101, 23 ),
		ang = Angle( 0, 180, 100 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -123, 21 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.029
	}
)

RegisterVehiclePlates( "transittdm",
	{
		pos = Vector( 0, 122, 25.5 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -112, 40 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.029
	}
)

RegisterVehiclePlates( "lmptdm",
	{
		pos = Vector( 0, 116, 10 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034
	},
	{
		pos = Vector( 0, -112, 17 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.029
	}
)
