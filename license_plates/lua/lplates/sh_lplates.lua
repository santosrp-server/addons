LPlates = {}

local vehicle_offsets = {}

function LPlates.RegisterVehiclePlates( vname, ... )
	vehicle_offsets[vname] = { ... }
end

function LPlates.GetVehiclePlates( vname )

	local vdata = GAMEMODE.Cars:GetCarByUID( vname )
	
	if !vdata then return end	
	
	local plates = vdata["LPlates"]
	
	if !plates then return end

	return plates
end
