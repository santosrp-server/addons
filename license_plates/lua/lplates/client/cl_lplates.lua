
surface.CreateFont( "PlateFontBig", {
	font = "Trebuchet",
	size = 128,
	antialias = true,
} )

surface.CreateFont( "PlateFontSmall", {
	font = "Trebuchet",
	size = 48,
	antialias = true,
} )

local pwidth, pheight = 512, 256 --plate dimensions, makes positioning easier
local plateMat = Material( "dan/plate.png" )

local pi = math.pi

local function drawCircle( x, y, rad, quality, col )
	surface.SetDrawColor( col )

	local step = (2 * pi) / quality
	local verts = {}
	local ang

	for i=1,quality do
		ang = i * step

		table.insert( verts, {
			x = x + math.cos( ang ) * rad,
			y = y + math.sin( ang ) * rad
		} )
	end

	surface.DrawPoly( verts )
end

local function drawScrew( cx, cy, rad )
	drawCircle( cx, cy, rad, 10, Color( 0, 0, 0, 255 ) )
	drawCircle( cx, cy, rad - 2, 10, LPlates.ScrewColor )

	surface.SetDrawColor( Color( 0, 0, 0, 255 ) )
	surface.DrawRect( cx - rad + 2, cy - 1, (rad * 2) - 4, 2 )
end

hook.Add( "PostDrawTranslucentRenderables", "cl_lplates_drawplates", function( depth, sky )
	if sky or depth then return end
	
	for _,veh in pairs( ents.GetAll() ) do
		if !IsValid( veh ) or
			!veh:IsVehicle() or
				veh:GetPos():Distance( LocalPlayer():GetShootPos() ) > LPlates.DistanceFade then continue end

		local vname = veh:GetNWString( "veh_name" )
		local vdata = GAMEMODE.Cars:GetCarByUID( vname )
		
		//print( "VNAME = "..vname )
		
		if !vdata then continue end
		
		local plates = vdata["LPlates"]

		if !plates then continue end

		local pserial = veh:GetNWString( "plate_serial" )

		if !pserial or string.len( pserial ) < 1 then continue end

		render.SuppressEngineLighting( true )

		for _,pinfo in pairs( plates ) do
			cam.Start3D2D( 
				veh:LocalToWorld( pinfo.pos ), 
				veh:LocalToWorldAngles( pinfo.ang ), 
				pinfo.scale 
			)
				surface.SetMaterial( plateMat )
				surface.SetDrawColor( LPlates.PlateColor )
				surface.DrawTexturedRect( -pwidth / 2 - 6, -pheight / 2 - 6, pwidth + 12, pheight + 12 )

				if LPlates.DrawTextShadow then
					draw.SimpleText( 
						pserial, "PlateFontBig",
						-4, 4, 
						LPlates.PlateTextSerialShadowColor,
						TEXT_ALIGN_CENTER,
						TEXT_ALIGN_CENTER
					)

					draw.SimpleText( 
						LPlates.PlateTextTop, "PlateFontSmall",
						-1, (-pheight / 2.6) + 1, 
						LPlates.PlateTextTopShadowColor,
						TEXT_ALIGN_CENTER,
						TEXT_ALIGN_CENTER
					)

					draw.SimpleText( 
						LPlates.PlateTextBottom, "PlateFontSmall",
						-1, (pheight / 2.7) + 1, 
						LPlates.PlateTextBottomShadowColor,
						TEXT_ALIGN_CENTER,
						TEXT_ALIGN_CENTER
					)
				end

				draw.SimpleText( 
					pserial, "PlateFontBig",
					0, 0, 
					LPlates.PlateTextSerialColor,
					TEXT_ALIGN_CENTER,
					TEXT_ALIGN_CENTER
				)

				draw.SimpleText( 
					LPlates.PlateTextTop, "PlateFontSmall",
					0, -pheight / 2.6, 
					LPlates.PlateTextTopColor,
					TEXT_ALIGN_CENTER,
					TEXT_ALIGN_CENTER
				)

				draw.SimpleText( 
					LPlates.PlateTextBottom, "PlateFontSmall",
					0, pheight / 2.7, 
					LPlates.PlateTextBottomColor,
					TEXT_ALIGN_CENTER,
					TEXT_ALIGN_CENTER
				)

				surface.SetDrawColor( LPlates.PlateDividerColor )

				surface.DrawRect( -pwidth / 2 + 8, -pheight / 3.7, pwidth - 16, 6 )
				surface.DrawRect( -pwidth / 2 + 8, pheight / 3.7, pwidth - 16, 6 )

				if LPlates.DrawScrews then
					local xoff, yoff = pwidth / 3.3, pheight / 2.55

					surface.SetTexture( 0 )

					drawScrew( xoff, yoff, 10 )
					drawScrew( -xoff, yoff, 10 )
					drawScrew( xoff, -yoff, 10 )
					drawScrew( -xoff, -yoff, 10 )
				end
			cam.End3D2D()
		end

		render.SuppressEngineLighting( false )
	end
end )
